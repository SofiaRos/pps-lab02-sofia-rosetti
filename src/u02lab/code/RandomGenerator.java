package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private int n;
    private int counter = 0;

    public RandomGenerator(int n){
        /*for (int i = 0; i < n; i++) {
            double value = Math.random();
            if (value > 0.5) {
                list.add(1);
            } else {
                list.add(0);
            }
        }*/
        this.n = n;
    }

    @Override
    public Optional<Integer> next() {
        if (counter < n) {
            counter++;
            double value = Math.random();
            return Optional.of(value > 0.5 ? 1 : 0);
        }
        return Optional.empty();
    }

    @Override
    public void reset() {
        this.counter = 0;
    }

    @Override
    public boolean isOver() {
        return (counter < n ? false : true);
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remainingElements = new ArrayList<>();
        for (int i = this.counter + 1; i <= n; i++){
            double value = Math.random();
            remainingElements.add(value > 0.5 ? 1 : 0);
        }
        return remainingElements;
    }
}
