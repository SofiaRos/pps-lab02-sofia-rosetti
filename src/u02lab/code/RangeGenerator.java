package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop has been produced, lead to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and gives false, at the end and gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private int start;
    private int stop;
    private int counter = 0;


    public RangeGenerator(int start, int stop){
        this.start = start;
        this.stop = stop;
    }

    @Override
    public Optional<Integer> next() {
        int next = 0;
        if (counter < (stop - start)) {
            if (counter == 0) {
                next = start + 1;
            } else {
                next++;
            }
            counter++;
            return Optional.of(next);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void reset() {
        this.counter = 0;
    }

    @Override
    public boolean isOver() {
        return this.counter == stop-start;
    }


    @Override
    public List<Integer> allRemaining() {
        List<Integer> remainingList = new ArrayList<>();
        int newStart = this.start + this.counter + 1;
        int remainingCounter = this.stop - this.start + 1 - this.counter - 1;
        for (int i = 0; i < remainingCounter; i++) {
            remainingList.add(newStart + i);
        }
        return remainingList;
    }
}
