package u02lab.code;

import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by sofia.rosetti on 07/03/2017.
 */
public class RandomGeneratorTest {
    @Test
    public void nextTest() throws Exception {
        RandomGenerator randomGenerator = new RandomGenerator(3);
        assertFalse(randomGenerator.next().equals(Optional.empty()));
        assertFalse(randomGenerator.next().equals(Optional.empty()));
        assertFalse(randomGenerator.next().equals(Optional.empty()));
        assertTrue(randomGenerator.next().equals(Optional.empty()));
    }

    @Test
    public void resetTest() throws Exception {
        RandomGenerator randomGenerator = new RandomGenerator(3);
        assertFalse(randomGenerator.next().equals(Optional.empty()));
        assertFalse(randomGenerator.next().equals(Optional.empty()));
        assertFalse(randomGenerator.next().equals(Optional.empty()));
        randomGenerator.reset();
        assertFalse(randomGenerator.next().equals(Optional.empty()));
    }

    @Test
    public void isOverTest() throws Exception {
        RandomGenerator randomGenerator = new RandomGenerator(3);
        assertFalse(randomGenerator.next().equals(Optional.empty()));
        assertFalse(randomGenerator.next().equals(Optional.empty()));
        assertFalse(randomGenerator.next().equals(Optional.empty()));
        assertTrue(randomGenerator.next().equals(Optional.empty()));
    }

    @Test
    public void allRemainingTest() throws Exception {
        RandomGenerator randomGenerator = new RandomGenerator(3);
        randomGenerator.next();
        List<Integer> testList = randomGenerator.allRemaining();
        assertTrue(testList.size() == 2);
    }

}