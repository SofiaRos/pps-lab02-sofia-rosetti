package u02lab.code;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by sofia.rosetti on 07/03/2017.
 */
public class RangeGeneratorTest {

    @Test
    public void nextTest() throws Exception {
        RangeGenerator rangeGenerator = new RangeGenerator(6,10);
        Optional<Integer> next = rangeGenerator.next();
        assertTrue(next.get().equals(7));
    }

    @Test
    public void resetTest() throws Exception {
        RangeGenerator rangeGenerator = new RangeGenerator(6,10);
        rangeGenerator.next();
        rangeGenerator.next();
        rangeGenerator.reset();
        assertTrue(rangeGenerator.next().equals(Optional.of(7)));
    }

    @Test
    public void isOverTest() throws Exception {
        RangeGenerator rangeGenerator = new RangeGenerator(6,10);
        rangeGenerator.next();
        rangeGenerator.next();
        assertFalse(rangeGenerator.isOver());
        rangeGenerator.next();
        rangeGenerator.next();
        assertTrue(rangeGenerator.isOver());
    }

    @Test
    public void allRemainingTest() throws Exception {
        RangeGenerator rangeGenerator = new RangeGenerator(6,10);
        rangeGenerator.next();
        rangeGenerator.next();

        List<Integer> myTestList = new ArrayList<>();
        myTestList.add(9);
        myTestList.add(10);
        //System.out.println(myTestList.toString());

        List<Integer> remainingList = rangeGenerator.allRemaining();
        //System.out.println(remainingList.toString());

        assertTrue(remainingList.equals(myTestList));
    }

    @Test
    public void nextAfterStopTest() throws Exception {
        RangeGenerator rangeGenerator = new RangeGenerator(2,3);
        rangeGenerator.next();
        Optional<Integer> next = rangeGenerator.next();
        assertTrue(next.equals(Optional.empty()));
    }

}